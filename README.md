# Beam

This is the master repo for all packages related to the Beam e-commerce site. It includes the following:

## `beam-cms`
This is a CraftCMS instance, hosted on Fortrabbit, that includes a bundled version of the frontend for CMS preview purposes.

## `beam-frontend`
This is a statically build, JS hydrated frontend. Webpack hosts the local development version. Static assets are built and deployed to Netlify.

----

## Local Development

_Individual setup and installation is documented on a per package basis._


## Deploying to Production

1. Add Fortrabbit as a GIT remote, named `production`.
2. Run the `deploy.sh` script. Your GIT password is the fortrabbit account password.

```
$ sh scripts/deploy.sh
```

This will do a few things - compress assets, commit those as a new deployment to the repo, and push to the `production` branch. This will trigger a Netlify build as well.