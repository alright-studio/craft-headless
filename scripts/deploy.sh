#!/usr/bin/env bash

# Build Frontend
echo Building frontend... 
cd ./packages/beam-frontend
yarn build:app

# Copy Frontend to CMS
echo Copying frontend to CMS...
cd ../../
rm -rf ./packages/beam-cms/web/dist/
cp -a ./packages/beam-frontend/dist/. ./packages/beam-cms/web/dist/

# Commit to master
echo Commiting deploy...
git add -A && git commit -m "Deploy - `date`"

# Push to master/production
echo Pushing deploy...
git push production master
git push origin master