<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see craft\config\GeneralConfig
 */

return [
    // Global settings
    '*' => [
        'defaultWeekStartDay' => 0,
        'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
        'cpTrigger' => 'admin',
        'securityKey' => getenv('SECURITY_KEY'),
        'maxUploadFileSize' => 67108864, // 64MB
        'assetHost' => getenv('ASSET_HOST'),
        'assetPath' => getenv('ASSET_PATH'),
        'imgixHost' => getenv('IMGIX_HOST'),
        'uploadsHost' => getenv('UPLOADS_HOST'),
    ],
    'dev' => [
        'siteUrl' => null,
        'devMode' => true,
        'disabledPlugins' => ['webhooks'],
    ],
    'staging' => [
        'siteUrl' => null,
    ],
    'production' => [
        'siteUrl' => null,
    ],
];
