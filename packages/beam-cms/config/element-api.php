<?php

use craft\elements\Entry;
use craft\helpers\UrlHelper;
use superbig\imgix\Imgix;

return [
	'endpoints' => [
		//
		// Get page by section handle
		//
		'api/pages/<pageHandle:\S+>' => function($pageHandle) {
			$draftId = Craft::$app->request->getQueryParam('draftId');
			$versionId = Craft::$app->request->getQueryParam('versionId');

			return [
				'elementType' => Entry::class,
				'criteria' => ['section' => $pageHandle],
				'one' => true,
				'transformer' => function(craft\elements\Entry $entry) {
					return [
						'id' => $entry->id,
						'title' => $entry->title,
						'slug' => $entry->slug,
					];
				},
			];
		},
	]
];