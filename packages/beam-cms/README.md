# Beam CMS

## Requirements
- MAMP
- PHP 5+
- MySQL 5+

## Local Development

*Local development requires MAMP.*

1. Install Composer dependencies
```
$ composer install
```
2. Start MAMP and create a new host (`beam.local`) and a database on `localhost`. Database backups can be found in Dropbox.
3. Copy `.env.example` to `.env` and set proper database configuration.
4. Navigate to your host (`http://beam.local`) in the browser.