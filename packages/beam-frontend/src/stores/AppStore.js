import createStore from '../utils/createStore';

//
// === Initiate State ===
//

export const DEFAULT_STATE = {
	// LIVE PREVIEW
	isDraftPreview: false,
	requestUrl: null,
	// LIVE PREVIEW

	isRouteLoading: false,
	pageBySlug: {},
};

//
// === Actions ===
//

const onRouteChangeStart = store => () => {
	store.setState({isRouteLoading: true});
};

const onRouteChangeEnd = store => () => {
	store.setState({isRouteLoading: false});
};

//
// === Create Store ===
//

export default (initialState = {}) => {
	const store = createStore({
		...DEFAULT_STATE,
		...initialState,
	});
	
	return {
		// Interface
		getState: store.getState,
		setState: store.setState,
		listen: store.listen,
		unlisten: store.unlisten,

		// Actions
		onRouteChangeStart: onRouteChangeStart(store),
		onRouteChangeEnd: onRouteChangeEnd(store),
	};
};