import {matchPath} from 'react-router';

//
// === Route Config ===
//

const home = {
	props: {
		path: '/',
		exact: true,
	},
	getComponent() {
		return import(
			/* webpackChunkName: 'route-home' */
			'../routes/Home'
		).then(({default: Home}) => Home);
	},
	getStateRequest() {
		return import(
			/* webpackChunkName: 'data-requirements' */
			'./dataRequirements'
		).then(({default: dataRequirements}) => dataRequirements.home);
	},
	setState: (state, response) => ({
		...state,
		pageBySlug: {
			...state.pageBySlug,
			[response.slug]: response,
		}
	})
};

const about = {
	props: {
		path: '/about',
		exact: true,
	},
	getComponent() {
		return import(
			/* webpackChunkName: 'route-about' */
			'../routes/About'
		).then(({default: About}) => About);
	},
	getStateRequest() {
		return import(
			/* webpackChunkName: 'data-requirements' */
			'./dataRequirements'
		).then(({default: dataRequirements}) => dataRequirements.about);
	},
	setState: (state, response) => ({
		...state,
		pageBySlug: {
			...state.pageBySlug,
			[response.slug]: response,
		}
	})
};

//
// === Export all routes for iterating ===
//

// Order matters here
export const allRoutes = [
	home,
	about,
];

//
// === Export Interfces ===
//

export const getRoute = path => {
	for (let i = 0; i < allRoutes.length; i++) {
		const routeProps = allRoutes[i].props;

		if (matchPath(path, routeProps)) {
			return allRoutes[i];
		}
	}

	return null;
};

export default {
	//
	// === Home ===
	//
	home,
	about,
};