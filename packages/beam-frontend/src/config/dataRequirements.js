const addPreviewToken = path => {
	let newPath = path;

	if (global && global.location && global.location.href) {
		const m = global.location.href.match(/\btoken=([^&]+)/);
		const token = m ? m[1] : '';

		newPath += `?token=${token}`;
	}

	return newPath;
};

const getPageByHandle = handle => () => {
	return global.fetch(addPreviewToken(`${CONFIG.API_HOST}/api/pages/${handle}`))
		.then(response => response.json());
};

export default {
	//
	// === Pages ===
	//
	home: getPageByHandle('home'),
	about: getPageByHandle('about'),
};