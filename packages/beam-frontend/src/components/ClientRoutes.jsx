import React from 'react';
import {Route, Switch} from 'react-router';
import LazyComponent from './LazyComponent';
import Helmet from 'react-helmet';
import routeConfig from '../config/routes';
import withProps from 'recompose/withProps';
import {connectToStore} from './Provider';

const ClientRoutes = ({
	isDraftPreview,
	requestUrl,
}) => {
	// Allow live preview
	const location = isDraftPreview ? {pathname: requestUrl} : undefined;

	return (
		<React.Fragment>
			{/* Default Title Template */}
			<Helmet
				defaultTitle="Beam"
				titleTemplate="%s | Beam"
			/>

			{/* Main Router */}
			<Switch location={location}>
				<Route
					{...routeConfig.home.props}
					component={withProps({
						getComponent: routeConfig.home.getComponent,
					})(LazyComponent)}
				/>
				<Route
					{...routeConfig.about.props}
					component={withProps({
						getComponent: routeConfig.about.getComponent,
					})(LazyComponent)}
				/>
			</Switch>
		</React.Fragment>
	);
};

const mapStateToProps = ({
	isDraftPreview,
	requestUrl,
}) => ({
	isDraftPreview,
	requestUrl,
});

export default connectToStore(mapStateToProps)(ClientRoutes);