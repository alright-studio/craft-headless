//
// === Static/SPA Link ===
//
// This is the link component responsible for all the magic. 
// In preview contexts, we get our data requirements on the fly,
// but in a static context, we use the hydrated manifest to grab our
// data requirements.

import React, {Component} from 'react';
import LazyLink from './LazyLink';
import {getRoute} from '../config/routes';
import {connectToStore} from './Provider';
import emptyPromise from '../utils/emptyPromise';
const {__MANIFEST__} = global;

class Link extends Component {
	getRequirements = () => {
		if (__MANIFEST__.context === 'preview') {
			return this.getPreviewRequirements();
		} else if (__MANIFEST__.context === 'client') {
			return this.getClientRequirements();
		} else {
			return emptyPromise();
		}
	}

	getPreviewRequirements = () => {
		const {
			store,
			to
		} = this.props;
		const route = getRoute(to);

		// If we don't have a route, move on
		if (!route) return emptyPromise();

		// Set state in store after fetching
		const stateRequestPromise = route.getStateRequest()
			.then(fetchRouteState => fetchRouteState())
			.then(response => response.json())
			.then(response => {
				const currentState = store.getState();
				store.setState(route.setState(currentState, response));
			});

		// Return promise for data and component
		return Promise.all([
			route.getComponent(),
			stateRequestPromise,
		]);
	};

	getClientRequirements = () => {
		const {
			store,
			to
		} = this.props;
		const route = getRoute(to);

		if (!route) return emptyPromise();

		const statePath = __MANIFEST__.stateByPathname[to];
		let stateRequestPromise = emptyPromise;

		// Set state promise if we have a data path
		if (statePath) {
			stateRequestPromise = global.fetch(statePath)
				.then(response => response.json())
				.then(response => {
					const currentState = store.getState();
					store.setState(route.setState(currentState, response));
				});
		}

		// Return promise for data and component
		return Promise.all([
			route.getComponent(),
			stateRequestPromise,
		]);
	};

	render() {
		const {
			/* eslint-disable */
			store,
			onRouteChangeStart,
			onRouteChangeEnd,
			/* eslint-enable */
			...props
		} = this.props;

		return (
			<LazyLink
				{...props}
				getRequirements={this.getRequirements}
			/>
		);
	}
}

const mapStateToProps = () => ({});

const mapStoreToProps = store => ({
	store,
	onRouteChangeStart: store.onRouteChangeStart,
	onRouteChangeEnd: store.onRouteChangeEnd,
});

export default connectToStore(mapStateToProps, mapStoreToProps)(Link);