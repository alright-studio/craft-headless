import React, {Component} from 'react';

export default class LazyComponent extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isLoaded: false,
			isError: false,
			LoadedComponent: null,
		};
	}

	componentWillMount = () => {
		this.props.getComponent()
			.then(LoadedComponent => {
				this.setState({
					isLoaded: true,
					LoadedComponent,
				});
			})
			.catch(error => {
				this.setState({isError: true});
				console.log(error);
			});
	};

	render = () => {
		const {
			isError,
			isLoaded,
			LoadedComponent,
		} = this.state;

		const {
			getComponent, // eslint-disable-line
			LoadingComponent,
			ErrorComponent,
			...props
		} = this.props;

		// If we had an error loading the page, show the error component
		if (isError) {
			return ErrorComponent ? <ErrorComponent {...props} /> : null;
		}

		// If we're not loaded, return a loader
		if (!isLoaded) {
			return LoadingComponent ? <LoadingComponent {...props} /> : null;
		}

		// Otherwise return the real component
		return <LoadedComponent {...props} />;
	};
}