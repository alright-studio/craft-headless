import React, {Component} from 'react';
import {withRouter} from 'react-router';
import emptyPromise from '../utils/emptyPromise';

class LazyLink extends Component {
	static defaultProps = {
		getRequirements: emptyPromise,
		onNavigateStart() {},
		onNavigateSuccess() {},
		onNavigateError() {},
	};

	constructor(props) {
		super(props);
	}

	onClick = event => {
		// Give event to parent before anything else
		if (this.props.onClick) {
			this.props.onClick(event);
		}

		// Same link, don't do anything
		if (event.target.href === global.location.href) {
			event.preventDefault();
			return;
		}

		// Prevent route matching under certain circumstances
		if (!global.history.pushState) return;
		if (event.which > 1 || event.metaKey || event.ctrlKey || event.shiftKey || event.altKey) return;
		if (event.target.target && event.target.target === '_blank') return;
		if (event.target.protocol !== global.location.protocol || event.target.hostname !== global.location.hostname) return;
		if (event.target.href.indexOf('#') > -1) return;
		if (event.target.getAttribute && typeof event.target.getAttribute('download') === 'string') return;

		// Found element, prevent default
		event.preventDefault();
		event.stopPropagation();

		const {
			to,
			history,
			getRequirements,
			onNavigateStart,
			onNavigateSuccess,
			onNavigateError,
		} = this.props;

		onNavigateStart();

		getRequirements()
			.then(() => {
				history.push(to);
				onNavigateSuccess(to);
			})
			.catch(error => {
				console.log(error);
				onNavigateError(error);
			});
	};

	render() {
		const {
			/* eslint-disable */
			onClick,
			history,
			location,
			staticContext,
			match,
			getRequirements,
			onNavigateStart,
			onNavigateSuccess,
			onNavigateError,
			/* eslint-enable */
			to,
			...props
		} = this.props;

		return (
			<a
				onClick={this.onClick}
				href={to}
				{...props}
			/>
		);
	}
}

export default withRouter(LazyLink);