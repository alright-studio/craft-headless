import React from 'react';
import {Route, Switch} from 'react-router';
import Home from '../routes/Home';
import About from '../routes/About';
import Helmet from 'react-helmet';
import routeConfig from '../config/routes';

export default () => (
	<React.Fragment>
		{/* Default Title Template */}
		<Helmet
			defaultTitle="Beam"
			titleTemplate="%s | Beam"
		/>

		{/* Main Router */}
		<Switch>
			<Route
				{...routeConfig.home.props}
				component={Home}
			/>
			<Route
				{...routeConfig.about.props}
				component={About}
			/>
		</Switch>
	</React.Fragment>
);