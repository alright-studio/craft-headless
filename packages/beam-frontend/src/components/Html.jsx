import React from 'react';

export default ({
	helmet,
	html,
	assetHost,
	assetPath,
}) => {
	const htmlAttrs = helmet.htmlAttributes.toComponent();
	const bodyAttrs = helmet.bodyAttributes.toComponent();

	return (
		<html {...htmlAttrs}>
			<head>
				{helmet.title.toComponent()}
				{helmet.meta.toComponent()}
				{helmet.link.toComponent()}
			</head>
			<body {...bodyAttrs}>
				<div
					id="app-mount"
					dangerouslySetInnerHTML={{__html: html}}
				/>
				{helmet.script.toComponent()}
			</body>
		</html>
	);
};