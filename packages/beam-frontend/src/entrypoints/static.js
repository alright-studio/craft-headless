// Polyfill fetch in node env.
import fetch from 'node-fetch';
global.fetch = fetch;

import React from 'react';
import {renderToString} from 'react-dom/server';
import {StaticRouter as Router} from 'react-router-dom';
import AppStore, {DEFAULT_STATE} from '../stores/AppStore';
import StaticRoutes from '../components/StaticRoutes';
import dataRequirements from '../config/dataRequirements';
import routes from '../config/routes';
import Provider from '../components/Provider';
import Html from '../components/Html';
import Helmet from 'react-helmet';

const DEFAULT_ASSET_CONFIG = {
	assetPath: '',
	assetHost: '',
};

//
// === Create page ===
//

const createPage = ({
	location = null,
	manifest = {},
	state = {},
	assetConfig = DEFAULT_ASSET_CONFIG,
}) => {
	// Create store instance
	const store = AppStore(state);

	// Create client asset dependencies
	// TODO: replace for dynamically pulling this in via webpack
	const assets = (
		<Helmet>
			<link
				rel="stylesheet"
				type="text/css"
				href={`${assetConfig.assetHost}${assetConfig.assetPath}client.css`}
			/>
			<script
				defer
				src={`${assetConfig.assetHost}${assetConfig.assetPath}client.js`}
				type="text/javascript"
			/>
		</Helmet>
	);

	// Add state hydration and manifest
	const hydration = (
		<Helmet>
			<script>{`var __HYDRATE__ = ${JSON.stringify(state)};`}</script>
			<script>{`var __MANIFEST__ = { context: 'client', stateByPathname: ${JSON.stringify(manifest)} };`}</script>
		</Helmet>
	);

	// Create page html
	const html = renderToString((
		<React.Fragment>
			{/* Asset Dependencies */}
			{hydration}
			{assets}
			
			{/* Render app */}
			<Provider store={store}>
				<Router
					location={location}>
					<StaticRoutes />
				</Router>
			</Provider>
		</React.Fragment>
	));

	// Gather head details
	const helmet = Helmet.renderStatic();

	return renderToString(
		<Html
			helmet={helmet}
			html={html}
		/>
	);
};

//
// === Static Interface ===
//

export default {
	createPage,
	dataRequirements,
	routes,
	initialState: DEFAULT_STATE,
};