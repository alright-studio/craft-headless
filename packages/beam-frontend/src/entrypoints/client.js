// Polyfills
import 'unfetch/polyfill';

import React from 'react';
import {hydrate} from 'react-dom';
import {hot} from 'react-hot-loader/root';
import {BrowserRouter as Router} from 'react-router-dom';
import AppStore from '../stores/AppStore';
import ClientRoutes from '../components/ClientRoutes';
import Provider from '../components/Provider';
import emptyPromise from '../utils/emptyPromise';
import {getRoute} from '../config/routes';

const {
	__HYDRATE__,
	__MANIFEST__,
} = global;

//
// === DOM Connection ===
//

const mountNode = document.getElementById('app-mount');

//
// === Create Store ===
//

const store = global.STORE = AppStore(__HYDRATE__);

//
// === Hydrate Preview state ===
//

const route = getRoute(__HYDRATE__.requestUrl ? __HYDRATE__.requestUrl : global.location.pathname);
let routePromise = route.getComponent();

if (__MANIFEST__.context === 'preview') {
	routePromise = Promise.all([
		route.getComponent(),
		route.getStateRequest()
	])
	.then(([{default: Component}, fetchRouteState]) => fetchRouteState())
	.then(response => {
		const currentState = store.getState();
		store.setState(route.setState(currentState, response));
	});
}

//
// === Create hot routes ===
//

const HotClientRoutes = hot(ClientRoutes);

//
// === Render app after state is hydrated ===
//

routePromise.then(() => {
	hydrate((
		<Provider store={store}>
			<Router>
				<HotClientRoutes />
			</Router>
		</Provider>
	), mountNode);
});