import React from 'react';
import {connectToStore} from '../components/Provider';
import Link from '../components/Link';

export const About = ({title}) => (
	<div>
		<h1>Hello world: {title}</h1>
		<ul>
			<li><Link to="/">Home</Link></li>
			<li><Link to="/about">About</Link></li>
		</ul>
	</div>
);

const mapStateToProps = ({
	pageBySlug
}) => (
	pageBySlug.about ? pageBySlug.about : {}
);

export default connectToStore(mapStateToProps)(About);