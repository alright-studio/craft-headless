const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

//
// === Constants ===
//

const ROOT_DIR = path.resolve(__dirname, '..');
const ENTRYPOINT_DIR = path.resolve(ROOT_DIR, 'src', 'entrypoints');
const CLIENT_DIST_DIR = path.resolve(ROOT_DIR, 'dist');
const STATIC_DIST_DIR = path.resolve(ROOT_DIR, 'dist', 'static');

// Configure environment
require('dotenv').config({
	path: path.resolve(ROOT_DIR, '.env.production')
});

const {
	ASSET_PATH,
	API_HOST,
} = process.env;

//
// === Sass Loader Options ===
//

const postCSSOptions = {
	plugins: () => [
		autoprefixer({
			browsers: [
				'>1%',
				'last 4 versions',
				'not ie < 9'
			]
		})
	]
};

const sassLoaderOptions = {
	includePaths: [
		path.resolve(ROOT_DIR, 'node_modules'),
	]
};

const sassLoader = {
	test: /\.(scss|css)$/,
	use: [
		MiniCssExtractPlugin.loader,
		'css-loader',
		{
			loader: 'postcss-loader',
			options: postCSSOptions
		},
		{
			loader: 'sass-loader',
			options: sassLoaderOptions
		}
	],
	include: path.resolve(ROOT_DIR, 'src')
};

//
// === JS Loaders ===
//

const jsxLoader = {
	test: /\.jsx?$/,
	loader: 'babel-loader',
	include: path.resolve(ROOT_DIR, 'src')
};

//
// === Fonts & Images ===
//

const fontImageLoader = {
	test: /\.(woff(2)?|ttf|otf|eot|png|jpg|jpeg|svg|gif)$/,
	loader: 'file-loader',
	options: {
		name: '[name].[ext]',
	},
};

//
// === Config ===
//

const staticConfig = {
	target: 'node',
	mode: 'production',
	entry: {
		'static': path.resolve(ENTRYPOINT_DIR, 'static.js'),
	},
	output: {
		path: STATIC_DIST_DIR,
		publicPath: ASSET_PATH,
		filename: '[name].js',
		chunkFilename: '[name].[chunkhash].js',
		library: {
			root: 'BeamStatic',
			amd: 'beam-static',
			commonjs: 'beam-static'
		},
		libraryTarget: 'umd'
	},
	resolve: {
		extensions: ['.js', '.jsx'],
	},
	module: {
		rules: [
			jsxLoader,
			sassLoader,
			fontImageLoader,
		]
	},
	optimization: {
		// minimizer: [
		// 	new TerserPlugin(),
		// 	new OptimizeCSSAssetsPlugin(),
		// ]
		minimize: false,
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '[name].css',
			chunkFilename: '[name].[chunkhash].css',
		}),
		new webpack.DefinePlugin({
			CONFIG: JSON.stringify({
				API_HOST,
			}),
		}),
		// Polyfill fetch for node environment
		new webpack.ProvidePlugin({
			fetch: 'node-fetch',
		}),
	],
};

const clientConfig = {
	mode: 'production',
	entry: {
		'client': path.resolve(ENTRYPOINT_DIR, 'client.js'),
	},
	output: {
		path: CLIENT_DIST_DIR,
		publicPath: ASSET_PATH,
		filename: '[name].js',
		chunkFilename: '[name].[chunkhash].js'
	},
	resolve: {
		extensions: ['.js', '.jsx'],
	},
	module: {
		rules: [
			jsxLoader,
			sassLoader,
			fontImageLoader,
		]
	},
	optimization: {
		minimizer: [
			new TerserPlugin(),
			new OptimizeCSSAssetsPlugin(),
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '[name].css',
			chunkFilename: '[name].[chunkhash].css',
		}),
		new webpack.DefinePlugin({
			CONFIG: JSON.stringify({
				API_HOST,
			}),
		}),
	],
};

module.exports = [
	clientConfig,
	staticConfig,
];