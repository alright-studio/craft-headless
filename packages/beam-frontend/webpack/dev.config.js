const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

//
// === Constants ===
//

const ROOT_DIR = path.resolve(__dirname, '..');
const ENTRYPOINT_DIR = path.resolve(ROOT_DIR, 'src', 'entrypoints');
const DIST_DIR = path.resolve(ROOT_DIR, 'dist');

// Configure environment
require('dotenv').config({
	path: path.resolve(ROOT_DIR, '.env.development')
});

const {
	WEBPACK_PORT,
	ASSET_PATH,
	ASSET_HOST,
	API_HOST,
} = process.env;

//
// === Sass Loader Options ===
//

const postCSSOptions = {
	plugins: () => [
		autoprefixer({
			browsers: [
				'>1%',
				'last 4 versions',
				'not ie < 9'
			]
		})
	]
};

const sassLoaderOptions = {
	includePaths: [
		path.resolve(ROOT_DIR, 'node_modules'),
	]
};

const sassLoader = {
	test: /\.(scss|css)$/,
	use: [
		'style-loader',
		'css-loader',
		{
			loader: 'postcss-loader',
			options: postCSSOptions
		},
		{
			loader: 'sass-loader',
			options: sassLoaderOptions
		}
	],
	include: path.resolve(ROOT_DIR, 'src')
};

//
// === JS Loaders ===
//

const eslintLoader = {
	test: /\.jsx?$/,
	loader: 'eslint-loader',
	enforce: 'pre',
	exclude: path.resolve(ROOT_DIR, 'node_modules'),
};

const jsxLoader = {
	test: /\.jsx?$/,
	loader: 'babel-loader',
	include: path.resolve(ROOT_DIR, 'src'),
};

//
// === Fonts & Images ===
//

const fontImageLoader = {
	test: /\.(woff(2)?|ttf|otf|eot|png|jpg|jpeg|svg|gif)$/,
	loader: 'file-loader',
};

//
// === HTML Loader ===
//

const handlebarLoader = {
	test: /\.hbs$/,
	loader: 'handlebars-loader',
};

//
// === Config ===
//

module.exports = {
	mode: 'development',
	entry: {
		'client': path.resolve(ENTRYPOINT_DIR, 'client.js'),
	},
	output: {
		path: DIST_DIR,
		publicPath: ASSET_HOST + ASSET_PATH,
		filename: '[name].js',
		chunkFilename: '[name].[chunkhash].js'
	},
	resolve: {
		extensions: ['.js', '.jsx'],
	},
	module: {
		rules: [
			eslintLoader,
			jsxLoader,
			sassLoader,
			fontImageLoader,
			handlebarLoader,
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.DefinePlugin({
			CONFIG: JSON.stringify({
				API_HOST,
			}),
		}),
	],
	devServer: {
		hot: true,
		inline: true,
		port: WEBPACK_PORT,
		contentBase: [
			path.resolve(ROOT_DIR, 'static'),
			path.resolve(ROOT_DIR, 'public'),
		],
		headers: {
			'Access-Control-Allow-Origin': '*'
		},
		overlay: true,
		disableHostCheck: true, // TODO: https://github.com/webpack/webpack-dev-server/issues/1604
	}
};