# Beam Frontend

This is the static rendered React frontend for the Beam e-commerce store.

## Requirements
- Node 8+
- Yarn

## Local Development

1. Install Yarn dependencies
```
$ yarn install
```
2. Copy `.example.env` to `.env.development` an add appropriate variables.
3. Start local development server to host development assets.
```
$ yarn start
```

## Static Build Process

Running `yarn build` runs the following sequence:

1. Webpack builds both `client` and `static` builds for use to render the site.
2. Renders out static page entrypoints according to the build script.
3. Copies `dist` and `static` folders to `public` directory.
4. Finally, hosts the `public` directory.

This spits out a public directory that is ready to be served by Netlify. 

*Important note: The local development process serves up this public directory entirely on the asset port. You can use this to test out static builds locally.*