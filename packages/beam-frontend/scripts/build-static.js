const {
	createPage,
	dataRequirements,
	initialState,
	routes: routeConfig,
} = require('../dist/static/static').default;
const path = require('path');
const fs = require('fs-extra');
const ora = require('ora');
const reduce = require('lodash/reduce');

const ROOT_DIR = path.resolve(__dirname, '..');
const STATIC_DIR = path.resolve(ROOT_DIR, 'static');
const HTML_HEADER = '<!DOCTYPE html>';

// Configure environment
require('dotenv').config({
	path: path.resolve(ROOT_DIR, '.env.production')
});

const {
	ASSET_HOST,
	ASSET_PATH,
} = process.env;


const assetConfig = {
	assetPath: ASSET_PATH,
	assetHost: ASSET_HOST,
};


//
// === Write Static Files ===
//

const writeHtmlPage = ({
	filePath,
	contents
}) => {	
	const indexFilePath = path.resolve(STATIC_DIR, filePath, 'index.html');

	fs.ensureFileSync(indexFilePath);
	fs.writeFileSync(indexFilePath, HTML_HEADER + contents);
};

const writeStateFile = ({
	filePath,
	state,
}) => {
	const stateFilePath = path.resolve(STATIC_DIR, filePath, 'data.json');

	fs.ensureFileSync(stateFilePath);
	fs.writeJsonSync(stateFilePath, state);
};

//
// === Build Sequence ===
//

const progress = ora().start();
progress.text = 'Fetching data';

const entrypoints = [
	{
		location: '/',
		dataRequirement: dataRequirements.home,
		setState: routeConfig.home.setState,
	},
	{
		location: '/about',
		dataRequirement: dataRequirements.about,
		setState: routeConfig.about.setState,
	}
];

//
// === 1. Fetch Data ===
//

const allStateRequests = entrypoints.map(entrypoint => entrypoint.dataRequirement());

//
// === 2. Create manifest ===
//

const manifest = reduce(entrypoints, (manifest, entrypoint) => {
	return {
		...manifest,
		[entrypoint.location]: `${entrypoint.location.replace(/\/$/, '')}/data.json`,
	};
}, {});

Promise.all(allStateRequests).then(allStateResponses => {
	//
	// === 3. Build Pages ===
	//
	
	progress.text = 'Creating pages';

	const pages = allStateResponses.map((response, index) => {
		const entrypoint = entrypoints[index];
		const state = entrypoint.setState(initialState, response);
		let filePath = entrypoint.location;
		
		if (filePath.charAt(0) === '/') {
			filePath = filePath.substr(1);
		}

		const contents = createPage({
			location: entrypoint.location,
			state,
			manifest,
			assetConfig,
		});

		return {
			filePath,
			state: response,
			contents,
		};
	});

	//
	// === 4. Write Pages ===
	//

	progress.text = 'Writing pages';

	pages.forEach(page => writeHtmlPage(page));

	//
	// === 5. Create State Endpoints ===
	//
	
	progress.text = 'Writing state endpoints';

	pages.forEach(page => writeStateFile(page));

	//
	// === Complete ===
	//
	
	progress.stop();
})
.catch(error => {
	console.log(error);

	progress.stop();
});